﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets {
    public class AngleIndicator : MonoBehaviour {

        public Text text;

        void Start() {
            SetAngle(-1);
        }

        public void SetAngle(int angle) {
            if (angle >= 0) {
                text.text = angle + "º";
            } else {
                text.text = "";
            }
        }
      
    }
}
