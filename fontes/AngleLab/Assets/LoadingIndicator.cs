﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets {
    public class LoadingIndicator : MonoBehaviour {

        public Image image;

        void Start() {
            SetProgress(0);
        }

        public void SetProgress(float progress) {
            image.fillAmount = progress / 100;
        }
      
    }
}
