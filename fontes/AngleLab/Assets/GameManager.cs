﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets {
    public class GameManager : MonoBehaviour {

        public MapGenerator mapGenerator;
        public PlayerManager player;
        public Transform mainCamera;
        public CurrentAngleIndicator currentAngleIndicator;
        public AngleIndicator angleIndicator;
        public Canvas hudCanvas;
        public Transform startRoomDoor;
        private Graph map;
        private Vertex currentVertex;
        private bool playerMoving = false;
        private List<Vertex> currentPath;
        private Vector3 initialPlayerPos;
        private bool finishedGame = false;

        void Start() {
            map = new Graph();
            map.firstVertex = new Vertex(21, 34);         

            Vertex v1 = new Vertex(52, 33);
            map.firstVertex.Connect(v1);

            Vertex v3 = new Vertex(31, 76);
            map.firstVertex.Connect(v3);

            Vertex v2 = new Vertex(62, 65);
            v1.Connect(v2);
            v3.Connect(v2);

            Vertex v4 = new Vertex(99, 19);
            v1.Connect(v4);

            Vertex v6 = new Vertex(94, 50);
            v4.Connect(v6);

            Vertex v5 = new Vertex(67, 104);
            v3.Connect(v5);
            v6.Connect(v5);

            Vertex v7 = new Vertex(35, 140);
            v5.Connect(v7);

            Vertex v8 = new Vertex(136, 29);
            v6.Connect(v8);

            Vertex v9 = new Vertex(120, 83);
            v6.Connect(v9);

            Vertex v10 = new Vertex(160, 60);
            v8.Connect(v10);
            v9.Connect(v10);

            Vertex v11 = new Vertex(157, 112);
            v9.Connect(v11);

            Vertex v12 = new Vertex(105, 126);
            v9.Connect(v12);

            Vertex v13 = new Vertex(216, 102);
            v10.Connect(v13);

            Vertex v14 = new Vertex(155, 140);
            v12.Connect(v14);

            Vertex v15 = new Vertex(105, 154);
            v12.Connect(v15);

            Vertex v16 = new Vertex(186, 126);
            v13.Connect(v16);
            v14.Connect(v16);

            Vertex v17 = new Vertex(136, 184);
            v14.Connect(v17);
            v15.Connect(v17);

            Vertex v18 = new Vertex(202, 174);
            v14.Connect(v18);

            map.vertices.Add(map.firstVertex, new List<Vertex>() { v1, v3 });
            map.vertices.Add(v1, new List<Vertex>() { map.firstVertex, v2, v4 });
            map.vertices.Add(v2, new List<Vertex>() { v1, v3 });
            map.vertices.Add(v3, new List<Vertex>() { map.firstVertex, v2, v5});
            map.vertices.Add(v4, new List<Vertex>() { v1, v6 });
            map.vertices.Add(v5, new List<Vertex>() { v3, v6, v7 });
            map.vertices.Add(v6, new List<Vertex>() { v4, v5, v8, v9 });
            map.vertices.Add(v7, new List<Vertex>() { v5 });
            map.vertices.Add(v8, new List<Vertex>() { v6, v10 });
            map.vertices.Add(v9, new List<Vertex>() { v6, v10, v11, v12 });
            map.vertices.Add(v10, new List<Vertex>() { v8, v9, v13 });
            map.vertices.Add(v11, new List<Vertex>() { v9 });
            map.vertices.Add(v12, new List<Vertex>() { v9, v14, v15 });
            map.vertices.Add(v13, new List<Vertex>() { v10, v16 });
            map.vertices.Add(v14, new List<Vertex>() { v14, v16, v17, v18 });
            map.vertices.Add(v15, new List<Vertex>() { v12, v17 });
            map.vertices.Add(v16, new List<Vertex>() { v13, v14 });
            map.vertices.Add(v17, new List<Vertex>() { v14, v15 });
            map.vertices.Add(v18, new List<Vertex>() { v14 });

            map.initialVertex = map.firstVertex;
            map.finalVertex = v18;

            mapGenerator.renderGraph(map);

            initialPlayerPos = player.transform.position;
            InitGame();
        }

        private void InitGame() {
            player.transform.position = initialPlayerPos;
            currentPath = map.shortest_path(map.initialVertex, map.finalVertex);
            Vector3 baseVector = new Vector2(mainCamera.forward.x, mainCamera.forward.z);
            float angle = Angle360(startRoomDoor.position - player.transform.position, baseVector);
            currentAngleIndicator.forwardVector = baseVector;
            angleIndicator.SetAngle((int) angle);
            finishedGame = false;
        }

        void PlayerStoppedMoving() {
            playerMoving = false;
            hudCanvas.gameObject.SetActive(true);
        }

        public void ShowNextAngle(Vector2 baseVector) {
            float angle = Angle360(currentPath[0].pos - currentVertex.pos, baseVector);
            currentAngleIndicator.forwardVector = baseVector;
            angleIndicator.SetAngle((int) angle);
        }

        public static float Angle360(Vector2 p1, Vector2 p2, Vector2 o = default(Vector2)) {
            Vector2 v1, v2;
            if (o == default(Vector2)) {
                v1 = p1.normalized;
                v2 = p2.normalized;
            } else {
                v1 = (p1 - o).normalized;
                v2 = (p2 - o).normalized;
            }
            float angle = Vector2.Angle(v1, v2);
            return Mathf.Sign(Vector3.Cross(v1, v2).z) < 0 ? (360 - angle) % 360 : angle;
        }

        public void GoToVertex(Vertex destinyVertex) {
            if (!playerMoving && !finishedGame) {
                var oldVertex = currentVertex;
                currentVertex = destinyVertex;
                if (currentVertex != map.finalVertex) {
                    if (currentVertex == currentPath[0]) {
                        currentPath.RemoveAt(0);
                    } else {
                        currentPath = map.shortest_path(currentVertex, map.finalVertex);
                    }
                    ShowNextAngle(currentVertex.pos - oldVertex.pos);
                } else {
                    finishedGame = true;
                }

                player.MoveTo(new Vector3(destinyVertex.pos.x, 3f, destinyVertex.pos.y));
                playerMoving = true;
                hudCanvas.gameObject.SetActive(false);
            }
        }

        public void StartGame() {
            hudCanvas.gameObject.SetActive(true);
            player.transform.position = new Vector3(map.initialVertex.pos.x, 3f, map.initialVertex.pos.y);
            currentVertex = map.initialVertex;
            player.onFinishedMoving.AddListener(PlayerStoppedMoving);
            ShowNextAngle(new Vector2(mainCamera.forward.x, mainCamera.forward.z));
        }

        public void Exit() {
            Application.Quit();
        }

        public void ResetGame() {
            InitGame();
        }

    }
}
