﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets {
    class Wall : MonoBehaviour {

        public float size;
        public float pillarDistance;
        public float rotateYDecor = 0;
        public Vector2 start_top;
        public Vector2 start_bottom;
        public Vector2 end_top;
        public Vector2 end_bottom;
        public GameObject[] pillars;
        public GameObject lamp;

        void Start() {
            Mesh m = new Mesh();
            m.name = "Scripted_Plane_New_Mesh";
            Vector3[] vertices = new Vector3[4];
            vertices[0] = new Vector3(start_top.x, start_top.y, 0.01f);
            vertices[3] = new Vector3(end_top.x, end_top.y, 0.01f);
            vertices[1] = new Vector3(start_bottom.x, start_bottom.y, 0.01f);
            vertices[2] = new Vector3(end_bottom.x, end_bottom.y, 0.01f);
            m.vertices = vertices;

            Vector2[] uv = new Vector2[4];
            uv[0] = new Vector2(0, 0);
            uv[1] = new Vector2(0, 1);
            uv[2] = new Vector2(1, 1);
            uv[3] = new Vector2(1, 0);
            m.uv = uv;

            int[] triangles = new int[6];
            triangles[0] = 0;
            triangles[1] = 1;
            triangles[2] = 2;
            triangles[3] = 0;
            triangles[4] = 2;
            triangles[5] = 3;
            m.triangles = triangles;
            m.RecalculateNormals();
            GetComponent<MeshFilter>().mesh = m;
            CreateDecorationObjects();
        }

        private void CreateDecorationObjects() {
            float length = Math.Abs(start_top.x - end_top.x) - 2;

            float numberOfPillarGaps = (float) Math.Round(length / pillarDistance);
            float actualPillarSpacing = length / numberOfPillarGaps;
            float numberOfPillars = numberOfPillarGaps + 1;

            float currentX = MapGenerator.ROOM_RADIUS + 1;
            for (var i = 0; i < numberOfPillars; i += 1) {
                GameObject newPillar = GameObject.Instantiate(GetRandomPillar());
                newPillar.transform.parent = this.transform;
                newPillar.transform.localPosition = new Vector3(currentX, 0.0f, 0.0f);
                newPillar.transform.localRotation = new Quaternion(0, 0, 0, 0);
                currentX += actualPillarSpacing;
            }

            float lampsDistance = pillarDistance * 2;
            float numberOfLampsGaps = (float) Math.Round(length / lampsDistance);
            float actualLampsSpacing = actualPillarSpacing * 2;
            float numberOfLamps = numberOfLampsGaps;

            currentX = MapGenerator.ROOM_RADIUS + 1 + (pillarDistance / 2);
            for (var i = 0; i < numberOfLamps; i += 1) {
                GameObject newLamp = GameObject.Instantiate(lamp);
                newLamp.transform.parent = this.transform;
                newLamp.transform.localPosition = new Vector3(currentX, 4.25f, 0.0f);
                newLamp.transform.localRotation = new Quaternion(0, 0, 0, 0);
                newLamp.transform.Rotate(newLamp.transform.up, rotateYDecor);
                currentX += actualLampsSpacing;
            }
        }

        private GameObject GetRandomPillar() {
            return pillars[UnityEngine.Random.Range(0, pillars.Length)];
        }

    }
}
