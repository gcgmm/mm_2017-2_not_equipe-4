﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets;
using System;

namespace Assets {
    public class MapGenerator : MonoBehaviour {

        public const int ROOM_RADIUS = 5;
        public const float ROOM_RADIUS_GAP = 0.6f;
        public const float END_ITEMS_OFFSET = 11.15f;

        public GameObject prefab;
        public GameObject wall;
        public GameObject floor;
        public GameObject roof;
        public GameObject door;
        public GameObject endRoomItemsPrefab;

        private HashSet<Tuple<Vertex, Vertex>> connectionsMap = new HashSet<Tuple<Vertex, Vertex>>();
        
        public void renderGraph(Graph g) {
            Vertex first = g.firstVertex;

            renderVertex(g, first, null);
        }

        private float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up) {
            Vector3 perp = Vector3.Cross(fwd, targetDir);
            float dir = Vector3.Dot(perp, up);

            if (dir > 0.0) {
                return 1.0f;
            } else if (dir < 0.0) {
                return -1.0f;
            } else {
                return 0.0f;
            }
        }

        private float getAngle360(Vector3 v1, Vector3 v2) {
            float angle = Vector3.Angle(v1, v2);
            return AngleDir(v1, v2, Vector3.up) < 0 ? -angle : angle;
        }

        private void renderVertex(Graph graph, Vertex v, Transform parentx) {
            GameObject vObj = GameObject.Instantiate(prefab);
            vObj.SetActive(true);
            vObj.transform.parent = this.transform;
            vObj.transform.position = new Vector3(v.pos.x, 0, v.pos.y);
            v.printed = true;

            foreach (Vertex vChild in v.connections) {
                if (connectionsMap.Contains(new Tuple<Vertex, Vertex>(v, vChild)) || connectionsMap.Contains(new Tuple<Vertex, Vertex>(vChild, v))) continue;

                GameObject corridor = new GameObject("corridor");
                corridor.transform.parent = this.transform;
                GameObject wallObj = GameObject.Instantiate(wall);
                GameObject wall2Obj = GameObject.Instantiate(wall);
                GameObject floorObj = GameObject.Instantiate(floor);
                GameObject roofObj = GameObject.Instantiate(roof);
                GameObject door1Obj = GameObject.Instantiate(door);
                GameObject door2Obj = GameObject.Instantiate(door);

                wallObj.SetActive(true);
                wall2Obj.SetActive(true);
                floorObj.SetActive(true);
                roofObj.SetActive(true);
                door1Obj.SetActive(true);
                door2Obj.SetActive(true);

                wallObj.transform.parent = corridor.transform;
                wallObj.transform.position = new Vector3(0, 0, -2.8f);
                wall2Obj.transform.parent = corridor.transform;
                wall2Obj.GetComponent<Wall>().rotateYDecor = 180;
                wall2Obj.transform.position = new Vector3(0, 0, 2.8f);

                floorObj.transform.parent = corridor.transform;
                floorObj.transform.position = new Vector3(0, 0, -2.8f);

                roofObj.transform.parent = corridor.transform;
                roofObj.transform.position = new Vector3(0, 5.8f, -2.8f);

                Vector3 newChildPos = new Vector3(vChild.pos.x, 0, vChild.pos.y);
                float distance = Vector3.Distance(vObj.transform.position, newChildPos);

                door1Obj.transform.parent = corridor.transform;
                door1Obj.transform.position = new Vector3(ROOM_RADIUS + ROOM_RADIUS_GAP, 0, 0);
                door1Obj.GetComponentInChildren<Door>().destinyVertex = vChild;
                door2Obj.transform.parent = corridor.transform;
                door2Obj.transform.position = new Vector3(distance - ROOM_RADIUS - ROOM_RADIUS_GAP, 0, 0);
                door2Obj.GetComponentInChildren<Door>().destinyVertex = v;

                if (vChild == graph.finalVertex) {
                    GameObject finalRoomItems = GameObject.Instantiate(endRoomItemsPrefab);
                    finalRoomItems.SetActive(true);
                    finalRoomItems.transform.parent = corridor.transform;
                    finalRoomItems.transform.position = new Vector3(distance - ROOM_RADIUS - ROOM_RADIUS_GAP + END_ITEMS_OFFSET, 0, 0);
                }

                Wall wallComp = wallObj.GetComponent<Wall>();
                wallComp.start_bottom = new Vector2(ROOM_RADIUS + ROOM_RADIUS_GAP, 0);
                wallComp.start_top = new Vector2(ROOM_RADIUS + ROOM_RADIUS_GAP, 6);
                wallComp.end_bottom = new Vector2(distance - ROOM_RADIUS - ROOM_RADIUS_GAP, 0);
                wallComp.end_top = new Vector2(distance - ROOM_RADIUS - ROOM_RADIUS_GAP, 6);

                Wall wall2Comp = wall2Obj.GetComponent<Wall>();
                wall2Comp.end_bottom = new Vector2(ROOM_RADIUS + ROOM_RADIUS_GAP, 0);
                wall2Comp.end_top = new Vector2(ROOM_RADIUS + ROOM_RADIUS_GAP, 6);
                wall2Comp.start_bottom = new Vector2(distance - ROOM_RADIUS - ROOM_RADIUS_GAP, 0);
                wall2Comp.start_top = new Vector2(distance - ROOM_RADIUS - ROOM_RADIUS_GAP, 6);

                Floor floorComp = floorObj.GetComponent<Floor>();
                floorComp.end_bottom = new Vector2(ROOM_RADIUS + ROOM_RADIUS_GAP, 0);
                floorComp.end_top = new Vector2(ROOM_RADIUS + ROOM_RADIUS_GAP, 5.6f);
                floorComp.start_bottom = new Vector2(distance - ROOM_RADIUS - ROOM_RADIUS_GAP, 0);
                floorComp.start_top = new Vector2(distance - ROOM_RADIUS - ROOM_RADIUS_GAP, 5.6f);

                floorComp = roofObj.GetComponent<Floor>();
                floorComp.end_bottom = new Vector2(ROOM_RADIUS + ROOM_RADIUS_GAP, 5.6f);
                floorComp.end_top = new Vector2(ROOM_RADIUS + ROOM_RADIUS_GAP, 0);
                floorComp.start_bottom = new Vector2(distance - ROOM_RADIUS - ROOM_RADIUS_GAP, 5.6f);
                floorComp.start_top = new Vector2(distance - ROOM_RADIUS - ROOM_RADIUS_GAP, 0);

                float angle = getAngle360(Vector3.right, newChildPos - vObj.transform.position);
                corridor.transform.position = vObj.transform.position;
                corridor.transform.Rotate(corridor.transform.up, angle);

                connectionsMap.Add(new Tuple<Vertex, Vertex>(v, vChild));
                if (!vChild.printed) {
                    renderVertex(graph, vChild, vObj.transform);
                }
            }
        }

    }
}