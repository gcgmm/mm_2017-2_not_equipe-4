﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Assets {
    public class Timer : MonoBehaviour {

        public float seconds;
        public UnityEvent onTimeOver;
        public LoadingIndicator uiIndicator;
        private float actualSeconds;

        void Update() {
            if (actualSeconds > 0) {
                actualSeconds -= Time.deltaTime;
                uiIndicator.SetProgress(((seconds - actualSeconds) * 100) / seconds);
                if (actualSeconds <= 0) {
                    onTimeOver.Invoke();
                }
            }
        }

        public void StartTimer() {
            actualSeconds = seconds;
            uiIndicator.SetProgress(0);
        }

        public void EndTimer() {
            actualSeconds = 0;
            uiIndicator.SetProgress(0);
        }

    }
}
