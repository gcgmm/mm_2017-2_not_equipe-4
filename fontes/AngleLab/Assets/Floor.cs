﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets {
    class Floor : MonoBehaviour {

        public float size;
        public Vector2 start_top;
        public Vector2 start_bottom;
        public Vector2 end_top;
        public Vector2 end_bottom;

        void Start() {
            Mesh m = new Mesh();
            m.name = "Scripted_Plane_New_Mesh";
            Vector3[] vertices = new Vector3[4];
            vertices[0] = new Vector3(start_top.x, 0.01f, start_top.y);
            vertices[3] = new Vector3(end_top.x, 0.01f, end_top.y);
            vertices[1] = new Vector3(start_bottom.x, 0.01f, start_bottom.y);
            vertices[2] = new Vector3(end_bottom.x, 0.01f, end_bottom.y);
            m.vertices = vertices;

            Vector2[] uv = new Vector2[4];
            uv[0] = new Vector2(0, 0);
            uv[1] = new Vector2(0, 1);
            uv[2] = new Vector2(1, 1);
            uv[3] = new Vector2(1, 0);
            m.uv = uv;

            int[] triangles = new int[6];
            triangles[0] = 0;
            triangles[1] = 1;
            triangles[2] = 2;
            triangles[3] = 0;
            triangles[4] = 2;
            triangles[5] = 3;
            m.triangles = triangles;
            m.RecalculateNormals();
            GetComponent<MeshFilter>().mesh = m;
        }

    }
}
