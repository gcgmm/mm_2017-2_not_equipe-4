﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets {
    public class Door : MonoBehaviour {

        public GameManager gameManager;
        public Vertex destinyVertex;

        public void Enter() {
            gameManager.GoToVertex(destinyVertex);
        }

    }
}
