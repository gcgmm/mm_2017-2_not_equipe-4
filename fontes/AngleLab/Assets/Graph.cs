﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets {
    public class Graph {

        public Vertex firstVertex;
        public Dictionary<Vertex, List<Vertex>> vertices = new Dictionary<Vertex, List<Vertex>>();

        public Vertex initialVertex;
        public Vertex finalVertex;

        public List<Vertex> shortest_path(Vertex start, Vertex finish) {
            var previous = new Dictionary<Vertex, Vertex>();
            var distances = new Dictionary<Vertex, int>();
            var nodes = new List<Vertex>();

            List<Vertex> path = null;

            foreach (var vertex in vertices) {
                if (vertex.Key == start) {
                    distances[vertex.Key] = 0;
                } else {
                    distances[vertex.Key] = int.MaxValue;
                }

                nodes.Add(vertex.Key);
            }

            while (nodes.Count != 0) {
                nodes.Sort((x, y) => distances[x] - distances[y]);

                var smallest = nodes[0];
                nodes.Remove(smallest);

                if (smallest == finish) {
                    path = new List<Vertex>();
                    while (previous.ContainsKey(smallest)) {
                        path.Add(smallest);
                        smallest = previous[smallest];
                    }

                    break;
                }

                if (distances[smallest] == int.MaxValue) {
                    break;
                }

                foreach (var neighbor in vertices[smallest]) {
                    var alt = distances[smallest] + (int) Vector2.Distance(smallest.pos, neighbor.pos);
                    if (alt < distances[neighbor]) {
                        distances[neighbor] = alt;
                        previous[neighbor] = smallest;
                    }
                }
            }

            path.Reverse();
            return path;
        }

    }
}
