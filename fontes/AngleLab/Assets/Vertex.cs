﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets {
    public class Vertex {

        public Vector2 pos;
        public bool printed = false;
        public LinkedList<Vertex> connections = new LinkedList<Vertex>();

        public Vertex(float x, float y) {
            pos = new Vector2(x, y);
        }

        public void Connect(Vertex v) {
            connections.AddLast(v);
            v.connections.AddLast(this);
        }

    }
}
