﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets {
    public class CurrentAngleIndicator : MonoBehaviour {

        public Image image;
        public Transform cameraTransform;
        public Vector2 forwardVector;

        void Start() {
            SetAngle(0);
        }

        void Update() {
            SetAngle(Angle360(new Vector2(cameraTransform.forward.x, cameraTransform.forward.z), forwardVector));
        }

        public void SetAngle(float angle) {
            image.fillAmount = angle / 360;
        }

        public static float Angle360(Vector2 p1, Vector2 p2, Vector2 o = default(Vector2)) {
            Vector2 v1, v2;
            if (o == default(Vector2)) {
                v1 = p1.normalized;
                v2 = p2.normalized;
            } else {
                v1 = (p1 - o).normalized;
                v2 = (p2 - o).normalized;
            }
            float angle = Vector2.Angle(v1, v2);
            return Mathf.Sign(Vector3.Cross(v1, v2).z) < 0 ? (360 - angle) % 360 : angle;
        }
      
    }
}
