﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.Events;

namespace Assets {
    public class PlayerManager : MonoBehaviour {

        private const int LERP_VELOCITY = 10;

        public GameManager gameManager;
        public UnityEvent onFinishedMoving;
        private Vector3 moveTo;
        private bool isMoving;

        void Update() {
            if (isMoving) {
                transform.position = Vector3.MoveTowards(transform.position, moveTo, Time.deltaTime * LERP_VELOCITY);
                if (Vector3.Distance(transform.position, moveTo) < 0.2f) {
                    isMoving = false;
                    onFinishedMoving.Invoke();
                }
            }
        }


        public void MoveTo(Vector3 pos) {
            moveTo = pos;
            isMoving = true;
        }
    }
}
